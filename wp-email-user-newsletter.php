<?php
function weu_admin_send_newsletter(){
	global $current_user, $wpdb, $wp_roles;
    $user_roles = $current_user->roles;
    $roles = $wp_roles->get_names();
	if($user_roles[0]=='administrator') {
    wp_enqueue_script( 'wp-email-user-datatable-script', plugins_url('js/jquery.dataTables.min.js', __FILE__ ), array(), '1.0.0', false );
    wp_enqueue_script( 'wp-email-user-script', plugins_url('js/email-admin.js', __FILE__ ), array(), '1.0.0', false );
	wp_enqueue_style( 'wp-email-user-style', plugins_url('css/style.css', __FILE__ ) );
	wp_enqueue_style( 'wp-email-user-datatable-style', plugins_url('css/jquery.dataTables.min.css', __FILE__ ) );


	$wau_to = array();
	if( isset($_POST['ea_user_name'])){
		for($j=0;$j<count($_POST['ea_user_name']);$j++){
			$user= $_POST['ea_user_name'][$j];
			array_push($wau_to,$_POST[$user]);
		}
	}
	/* Send Mail to user using wp_mail */
	$wau_status = 2;
	$wau_too = array();

		$temp_key=isset($_POST['wau_sub'])?$_POST['wau_sub']:'';
   		$chk_val=isset($_POST['temp'])?$_POST['temp']:'';
		$table_name = $wpdb->prefix.'email_user';
   	if($chk_val==1)
			$wpdb->query($wpdb->prepare( "INSERT INTO `".$table_name."`(`template_key`, `template_value`, `status`) VALUES (%s,%s,%s)
				",
				$temp_key,stripcslashes($_POST['wau_mailcontent']),'template'));
	for($j=0;$j<count($wau_to);$j++){

		$table_name = $wpdb->prefix.'weu_subscribers';
		//Fetching Values from weu_subscribers table with unique email id $wau_to[$j]
		$curr_email_data = $wpdb->get_results( "SELECT * FROM $table_name	WHERE email = '$wau_to[$j]'	");
		foreach ($curr_email_data as $keyy) {
		//Calling function to generate link of unsubscribtion
		$list = 'default';
		$unsubscribe_link_for_email = weu_userUnsubscibe($keyy->id,$keyy->email,$list);
		//Replacing Tags like - [[unsubscribe]] with actual value from table like - $keyy->name (User Name)
		$replace= array(
				$unsubscribe_link_for_email,
				$keyy->name,
				$keyy->email,
  		);
		$find = array(
		'[[unsubscribe]]',
		'[[subscriber-name]]',
		'[[subscriber-email]]'
		);
		}
	    $mail_body = str_replace( $find, $replace, $_POST['wau_mailcontent'] );
        $subject = $_POST['wau_sub'];
		$body = stripslashes($mail_body);
		$from_email=sanitize_email($_POST['wau_from']);
   	    $from_name=sanitize_text_field($_POST['wau_from_name']);
	    sanitize_text_field( $body );
        $headers[] = 'From: '.$from_name.' <'. $from_email.'>';
	    $headers[] = 'Content-Type: text/html; charset="UTF-8"';
			$wau_to[$j]." - Echo wau_to</br>";
			$subject." - Subject</br>";
			$body." - Body</br>";
		$wau_status = wp_mail($wau_to[$j], $subject, $body, $headers);
	} // for ends
	if( $wau_status==1 ){
		echo '<div id="message" class="updated notice is-dismissible"><p>Your message has been sent successfully.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';
	} elseif( $wau_status==0 ){
		echo '<div id="message" class="updated notice is-dismissible error"><p> Sorry,your message has not sent.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';
	}
	$table_name = $wpdb->prefix.'weu_subscribers';
	$wau_subscriber = $wpdb->get_results( "SELECT * FROM $table_name" );
	echo "<div class='wrap'>";
	echo "<h2>Send Newsletter to Subscribers</h2>";
	echo "</div>";
	echo "<p>Send email to Subscribers who subscribed through WP Email Users Subscribe widget.</p>";
	echo '<form name="myform" class="wau_form" method="POST" action="#" onsubmit="return validation()" >';

	/* User role */
	echo '<table id="" class="form-table" >';
	echo '<tbody>';
	echo '<tr>';
	echo '<th>From Name</th> <td colspan="1"><input type="text" name="wau_from_name" value="'.$current_user->display_name.'" class="wau_boxlen"  id="wau_from_name" required></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<th>From Email</th> <td colspan="2"><input type="text" name="wau_from" value="'.$current_user->user_email.'" class="wau_boxlen"  id="wau_from" required></td>';
	echo '</tr>';
	/**
     * Select Users
     **/
    echo '<tr class="wau_user_toggle">';
    echo "<th><b>Send Email To &nbsp; </b></th>";
    echo '<td colspan="3">';

	echo '<table id="example" class="display alluser_datatable" cellspacing="0" width="100%">
	        <thead>
	            <tr style="text-align:left"> <th style="text-align:center" ><input name="select_all" value="1" id="example-select-all" class="select-all" type="checkbox"></th>
	                 <th>Display name</th>
	                 <th>Email</th>
	            </tr>
	        </thead>
	        <tbody>';
	foreach ( $wau_subscriber as $user ){
	echo '<tr style="text-align:left">';
	echo '<td style="text-align:center"><input type="checkbox" name="ea_user_name[]" value="'.$user->id.'" class="select-all"></td>';
	echo '<td><span id="getDetail">'. esc_html( $user->name ).'</span></td>';
	echo '<td><span >'.esc_html( $user->email ).'</span></td>';
	echo '</tr>';}
	echo'</tbody></table>'; // end user Data table for user
	foreach ( $wau_subscriber as $user  ) {
		//echo '<input type="hidden" name="total_count[]" value="' . esc_html( $user->ID ) . '">';
		echo  '<input type="hidden" name="' . esc_html( $user->id ) . '" value="'. esc_html( $user->email ) . '">';
	}

	/* select roles */
	$mail_content="";

	$table_name = $wpdb->prefix.'email_user';
    $myrows = $wpdb->get_results( "SELECT id, template_key, template_value FROM $table_name WHERE status = 'template'" );
	$template_path_one = plugins_url('template1.html', __FILE__ );
	$template_path_two = plugins_url('template2.html', __FILE__ );
	echo '<tr>';
	echo '<th>Template </th><td colspan="3"><select autocomplete="off" id="wau_template" name="mail_template[]" class="wau-template-selector" style="width:100%">
        <option selected="selected">- Select -</option>
        <option value="'.$template_path_one.'" id="wau_template_t1"> Default Template - 1 </option>
        <option value="'.$template_path_two.'" id="wau_template_t2"> Default Template - 2 </option>';
        for ($i=0;$i<count($myrows);$i++) {
         echo '<option value="'.$myrows[$i]->id.'" id="am" >'.$myrows[$i]->template_key.'</option>';
          }
       '</select></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<th>Subject</th> <td colspan="3"><input type="text" name="wau_sub" class="wau_boxlen"  id="sub" placeholder="write your email subject here" required></td>';
	echo '</tr>';
	echo '<tr>';
	echo '<th scope="row" valign="top"><label for="wau_mailcontent">Message</label></th>';
    echo '<td colspan="3">';
	echo '<div id="msg" class="wau_boxlen" name="wau_mailcontent">';
		wp_editor($mail_content, "wau_mailcontent",array('wpautop'=>false,'media_buttons' => true));
	echo '</div>';
    echo '<b> [[unsubscribe]] : </b>use this placeholder to add unsubscribe link. </br>
          <b> [[subscriber-name]] : </b>use this placeholder to display user name. </br>
          <b> [[subscriber-email]] : </b>use this placeholder to display user email.</br>';
   	echo '<input type="checkbox" value="1" name="temp">Save template</br>';
	echo '</td>';
	echo'</tr>';
	echo '<tr>';
	echo '<th></th>';
	echo '<td colspan="3">';
	echo '<div><input type="submit" value="Send" class="button button-hero button-primary" id="weu_send" ></div>';
	echo '</td>';
	echo '</tr>';
	echo '</tbody>';
	echo '</table>';
	echo '</form>';
}
}
?>
