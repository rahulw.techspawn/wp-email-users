function weu_submit_page(url) {
  var weu = jQuery.noConflict();
  weuSubemail = document.getElementById("weu_txt_email");
  weuSubname = document.getElementById("weu_txt_name");
  weuSubgroup = document.getElementById("weu_txt_group");
  var subemail = weuSubemail.value;
  var subname = weuSubname.value;
  var ajax_url = weu_widget_notices.weu_ajax_url;
    if( weuSubemail.value == "" ) {
        alert(weu_widget_notices.weuSubemail_notice);
        weuSubemail.focus();
        return false;    
    }
    if( weuSubemail.value!="" && ( weuSubemail.value.indexOf("@",0) == -1 || weuSubemail.value.indexOf(".",0) == -1 )) {
        alert(weu_widget_notices.weu_incorrect_email);
        weuSubemail.focus();
        weuSubemail.select();
        return false;
    }
      var data = {
        'action': 'weu_subscribe_users_nl',
        'sub_email' :subemail,
        'sub_name': subname
      };
      weu.post(ajax_url,data, function(response){
            if(response==="success"){
              document.getElementById("weu_msg").innerHTML = weu_widget_notices.weu_success_message;
            }
            else if(response==="exist"){
                  document.getElementById("weu_msg").innerHTML = weu_widget_notices.weuSubemail_exists;
            }
            else {
                document.getElementById("weu_msg").innerHTML = weu_widget_notices.weu_error;
            }
      });
}